
# ТОП 15 команд Git

## Номер 1
> git init

Создать пустой репозиторий или пересоздать существующий.

## Номер 2
> git config

Получить или установить параметр глобальной конфигурации.

Без некоторых настроенных параметров невозможно использовать важнейший функционал Git.
```
git config --global user.name "John Doe"
git config --global user.username "cyril2lambda@gmail.com"
```

## Номер 3
> git add

Добавить файлы в список отслеживаемых файлов.
```
git add *
git add .
git add --all
```

## Номер 4
> git commit

Записать отслеживаемые изменения в репозиторий.
```
git commit -a
git commit -m "Comment"
```

## Номер 5
> git log

Отобразить информацию о последних коммитах.
```
git log
git log -n 2
git log --oneline
```

## Номер 6
> git diff

Отобразить изменения.
```
git diff
git diff --no-index
git diff commit-name
```

## Номер 7
> git checkout

Восстановить файлы или переключиться на ветку.
```
git checkout hash-of-commit
```
По-умолчанию внесенные изменения попадают в область неотслеживаемых изменений.
```
git checkout hash-of-commit file.ext
```

## Номер 8
> git revert

Отменить коммит (сбросить внесенные изменения относительно коммита).
```
git revert
```
Создает новый комит, то есть история не теряется.

## Номер 9
> git reset

Сбросить текущий указатель до указанного коммита или удалить файл из области отслеживаемых файлов.

Например, удалить файл из отслеживаемой области.
```
git reset HEAD my-file.ext
git reset -- my-file.ext
```
Или очистить отслеживаемую область
```
git reset
```
Или **удалить** и **потерять** внесенные изменения.
```
git reset --hard
```

## Номер 10
> git branch

Отобразить ветки. Создать или удалить ветку.
```
git branch
git branch dev-branch
git branch -D remove-this-branch
```

## Номер 11
> git merge

Слить две и более Git-истории.

Например, слить `dev-branch` в текущую ветку.
```
git merge dev-branch
```
Интерактивный режим (полезно при squash-коммитах).
```
git merge
```

## Номер 12
> git rebase

Изменить "base" текущей ветки.
```
git rebase master
git rebase -i master
```

## Номер 13
> git stash

Работа со "стеком". Сохранить или получить изменения из структуры данных, подобной стеку.

Сохранить текущие изменения в стек (Push).
```
git stash
```
Отобразить стек.
```
git stash list
```
Получить знаечние из стека (Pop).
```
git stash apply
```

## Номер 14
> git remote

Управлять удаленными репозиториями.

Отобразить список удаленных репозиториев.
```
git remote
```
Добавить удаленный репозиторий/
```
git remote add repo-name https://link.git
```

## Номер 15
> git push
> git pull

Отправить и получить изменения из удаленного репозитория.

Отправить.
```
git push repo-name branch-name
```
Получить.
```
git pull repo-name brnach-name
```
